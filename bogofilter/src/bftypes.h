/*****************************************************************************/
/** \file bftypes.h
 * \brief Type definitions for bogofilter.
 *
 * This file shall define the bool and uint32_t types.
 * it shall include inttypes.h and stdbool.h if present.
 *
 * Parts were taken from autoconf.info.
 */
/*****************************************************************************/

#ifndef BFTYPES_H
#define BFTYPES_H

#ifndef	CONFIG_H
# define  CONFIG_H
# include "config.h"
#endif

#include <stdio.h>
#ifdef HAVE_SYS_TYPES_H
# include <sys/types.h>
#endif
#ifdef HAVE_SYS_STAT_H
# include <sys/stat.h>
#endif
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#ifdef HAVE_STRINGS_H
# include <strings.h>
#endif
#ifdef HAVE_INTTYPES_H
# include <inttypes.h>
#endif
#ifdef HAVE_STDINT_H
# include <stdint.h>
#endif
#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

/** Define C99 style _Bool type for C89 compilers. */
#if HAVE_STDBOOL_H
# include <stdbool.h>
#else
# if ! HAVE__BOOL
#  ifdef __cplusplus
typedef bool _Bool;
#  else
typedef unsigned char _Bool;
#  endif
# endif

# ifndef __cplusplus
/** alias C99-standard _Bool type to bool */
#  define bool _Bool
/** default value for false */
#  define false 0
/** default value for true */
#  define true 1
/* internal - marker that we have defined true/false */
#  define __bool_true_false_are_defined 1
# endif
#endif

#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

#ifdef HAVE_INTTYPES_H
#include <inttypes.h>
#elif HAVE_STDINT_H
#include <stdint.h>
#endif

/** alias for unsigned long */
#ifndef HAVE_ULONG
typedef unsigned long ulong;
#endif

/** alias for unsigned int */
#ifndef HAVE_UINT
typedef unsigned int uint;
#endif

#if !defined(HAVE_SSIZE_T)
#if sizeof int == sizeof size_t
typedef int ssize_t;
#elif sizeof long == sizeof size_t
typedef long ssize_t;
#endif
#endif

/** type for getrlimit/setrlimit functions (some systems don't define this
 * type) */
#ifndef HAVE_RLIM_T
typedef int rlim_t;
#endif

/** prevent db.h from redefining the types above */
#undef	__BIT_TYPES_DEFINED__
#define	__BIT_TYPES_DEFINED__ 1

/* splint crutch */
#ifdef __LCLINT__
#define false 0
#define true 1
#endif

/** Data type for a date stamp in YYYY*10000 + MM*100 + DD format */
typedef uint32_t YYYYMMDD;

/* sanity check */
#ifdef HAVE_SIZE_T
#if SIZEOF_INT > SIZEOF_SIZE_T
#error "int is wider than size_t. The current code is not designed to work on such systems and needs review."
#endif
#endif

typedef unsigned char byte;

#endif /* BFTYPES_H */
