/*
* NAME:
*    xmalloc.c -- front-end to standard heap manipulation routines, with error checking.
*
* AUTHOR:
*    Gyepi Sam <gyepi@praxis-sw.com>
*
*/

#include "config.h"

#include "xmalloc.h"

void *
xmalloc(size_t size){
    void *ptr;
    if (0 == size) size = 1;
    ptr = bf_malloc(size);
    if (ptr == NULL)
	xmem_error("xmalloc"); 
    return ptr;
}

void
xfree(void *ptr){
    if (ptr)
	bf_free(ptr);
}
