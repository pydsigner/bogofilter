/*****************************************************************************

NAME:
   textblock.h -- prototypes and definitions for textblock.c

******************************************************************************/

#ifndef	TEXTBLOCK_H_INCLUDED
#define	TEXTBLOCK_H_INCLUDED

#include "bftypes.h"

typedef struct textdata_s {
    struct textdata_s *next;
    size_t             size;
    byte              *data;
} textdata_t;

typedef struct textblock_s {
    textdata_t *head;
    textdata_t *tail;
}  textblock_t;

textdata_t *textblock_head(void);
void textblock_init(void);
void textblock_free(void);

void textblock_add(const byte *text, size_t size);

#endif	/* TEXTBLOCK_H_INCLUDED */
