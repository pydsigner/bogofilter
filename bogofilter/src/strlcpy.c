/*  strlcpy.c - length-limit strcpy from BSD API.
    Copyright (C) 2022  Matthias Andree

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <sys/types.h>
#include <string.h>
#include "system.h"

/*
   This is a reimplementation that defers to 
   standard string functions, on the assumption that libc
   strlen/strcpy/memcpy might be heavily optimized
   and using SIMD instructions (SSE, AVX) or CPU/compiler intrinsic
   string functions.
*/

/*
 * Copy src to string dst of size siz.  At most siz-1 characters
 * will be copied.  Always NUL terminates (unless siz == 0).
 * Returns strlen(src); if retval >= siz, truncation occurred.
 */
size_t
strlcpy(char *dst, const char *src, size_t siz)
{
	size_t srclen = strlen(src);
	size_t dstlen = srclen;
	
	if (siz) { /* only touch anything if there is an output buffer */
		if (dstlen >= siz) dstlen = siz - 1; /* short buffer, copy what fits */
		memcpy(dst, src, dstlen);
		dst[dstlen] = '\0';
	}
	return srclen;
}
