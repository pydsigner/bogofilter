/*****************************************************************************

NAME:
   qp.h -- prototypes and definitions for qp.c

******************************************************************************/

#ifndef	QP_H_INCLUDED
#define	QP_H_INCLUDED

#include "word.h"

enum	qp_mode { RFC2045=2045, RFC2047=2047 };
typedef enum qp_mode qp_mode;
uint	qp_decode(word_t *word, qp_mode mode);
bool	qp_validate(const word_t *word, qp_mode mode);

#endif	/* QP_H_INCLUDED */
