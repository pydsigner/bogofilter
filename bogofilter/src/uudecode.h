/*****************************************************************************

NAME:
   uudecode.h -- prototypes and definitions for uudecode.c

******************************************************************************/

#ifndef	UUDECODE_H_INCLUDED
#define	UUDECODE_H_INCLUDED

#include "word.h"

uint	uudecode(word_t *word);

#endif	/* UUDECODE_H_INCLUDED */
